
<!-- TOC -->

- [Apache IoTDB 分布式混沌测试框架](#apache-iotdb-分布式混沌测试框架)
    - [项目需求](#项目需求)
        - [系统 OOM](#系统-oom)
        - [长时间 Full GC](#长时间-full-gc)
        - [磁盘写满时设置 TTL 失败](#磁盘写满时设置-ttl-失败)
        - [三节点分布式中拔掉 1 节点网线后其他节点的读写延时均受影响](#三节点分布式中拔掉-1-节点网线后其他节点的读写延时均受影响)
        - [分布式稳定性](#分布式稳定性)
    - [混沌工程](#混沌工程)
        - [什么是混沌工程](#什么是混沌工程)
        - [为什么需要混沌工程](#为什么需要混沌工程)
        - [混沌工程实施原则](#混沌工程实施原则)
    - [框架调研](#框架调研)
        - [ChaosBlade](#chaosblade)
        - [ChaosMesh](#chaosmesh)
        - [总结](#总结)
    - [项目内容](#项目内容)
        - [Proposal](#proposal)
        - [主要工作](#主要工作)
    - [错误类型](#错误类型)
        - [CPU](#cpu)
            - [负载](#负载)
        - [磁盘](#磁盘)
            - [读写 io 负载](#读写-io-负载)
            - [填充](#填充)
        - [内存](#内存)
            - [占用](#占用)
        - [网络](#网络)
            - [延迟](#延迟)
            - [丢包](#丢包)
            - [重排序](#重排序)
            - [重复](#重复)
        - [进程](#进程)
            - [退出](#退出)
            - [暂停](#暂停)
        - [JVM](#jvm)
            - [OOM](#oom)
            - [Full GC](#full-gc)
    - [机器环境](#机器环境)
    - [部署方式](#部署方式)
    - [Apache IoTDB 部署工具](#apache-iotdb-部署工具)
    - [用例调研](#用例调研)
        - [TiDB](#tidb)
            - [Jepsen](#jepsen)
            - [ChaosMesh](#chaosmesh-1)
        - [CockroachDB](#cockroachdb)
            - [Jepsen](#jepsen-1)
        - [总结](#总结-1)
    - [测试方案](#测试方案)
    - [测试版本](#测试版本)
    - [测试参数](#测试参数)
        - [IoTDB](#iotdb)
        - [IoTDB-Benchmark](#iotdb-benchmark)
    - [测试结果](#测试结果)
        - [Baseline](#baseline)
        - [节点宕机](#节点宕机)
        - [节点假死](#节点假死)
        - [节点重启](#节点重启)
        - [网络分区](#网络分区)
    - [项目总结](#项目总结)
        - [项目产出](#项目产出)
        - [遇到的问题及解决方案](#遇到的问题及解决方案)
        - [项目完成质量](#项目完成质量)
        - [导师沟通](#导师沟通)
    - [参考资料](#参考资料)

<!-- /TOC -->
# Apache IoTDB 分布式混沌测试框架

## 项目需求

Apache IoTDB（物联网数据库）是一体化收集、存储、管理与分析物联网时序数据的软件系统。Apache IoTDB 采用轻量式架构，具有高性能和丰富的功能，并与 Apache Hadoop、Spark 和 Flink 等进行了深度集成，可以满足工业物联网领域的海量数据存储、高速数据读取和复杂数据分析需求。

截止 2021 年 8 月，Apache IoTDB 已在数十家公司运行，拥有上百个线上实例。

随着项目的迅速发展，Apache IoTDB 在实施过程中逐渐遇到了各种各样开发测试时未曾细致考虑过的 corner case。对于单机版而言，手动模拟有些 corner case 的成本较高，而且也不一定能够稳定复现。对于分布式版本而言，手动模拟有些 corner case 的成本更高，而且在每次发版前进行一个相对系统化的测试也十分有必要。因此，**如何通过技术手段来改善并尽量减少线上系统的 bug 是一件值得社区去关注的工作**。

以下将简单介绍几个 corner case 的例子，以让读者对目前 Apache IoTDB 遇到的问题现状有一个基本了解。

### 系统 OOM

线上系统在运行时，偶尔会因为客户的负载与系统配置或参数不符从而导致 OOM，此时对客户应有明确的 OOM 提示。

然而，当前的 IoTDB 版本中对于 OOM 的处理还不够完善，部分情况下并不会直接给用户返回 OOM，而是以另一种异常地形式反馈给用户，例如 NPE 等等。一方面这会打击新用户使用我们系统的信心，另一方面也增加了我们寻找根本原因的时间。因此，我们有必要尽可能完善当前系统对于 OOM 异常的处理，并向用户给出明确的提示。

对于这种需求，我们一方面需要进行代码上的设计和开发，另一方面也需要测试验证修复后的正确性。对于后者，尽管我们可以手动根据机器配置和负载来模拟一个可能产生 OOM 的场景，但这相对会比较费时费力，而且不一定能够稳定复现 OOM。因此，如果我们能够直接从 JVM 层注入 OOM 错误，这将使得我们能够迅速的验证修复版本的正确性，从而降低人力成本。

### 长时间 Full GC

对于 Java 写的数据库，GC 是生产环境中必须关注的一项指标。不论是单机还是分布式，我们需要对系统在长时间 Full GC 时的对外表现有一个基本了解，并就该表现是否可以接受，是否需要优化等进行进一步的讨论和设计。例如，在分布式场景下，如果某一个节点产生了超过 1 分钟的 Full GC，其他节点是否还需要向该节点进行数据的转发，即使明知很可能并无法得到回复。

与 OOM 相似，尽管我们可以手动根据机器配置和负载来模拟一个可能产生长时间 Full GC 的场景，但这肯定不如我们直接从 JVM 层注入 Full GC 的错误方便。后者将极大地降低修复人员的人力成本，使得其只需要关注发生长时间 Full GC 后的应对逻辑，而不需要关注如何才能产生长时间的 Full GC。

### 磁盘写满时设置 TTL 失败

目前系统在测试时偶然发现了一个 bug：在数据目录挂载的磁盘写满时，分布式对存储组设置 TTL 会失败。

为了模拟这种场景，我们手动启动了近一周的写入长测来模拟磁盘写满的场景，接着才开始检测该 bug 是否能够稳定复现，这无疑增加了许多时间成本。

尽管我们能够写一些创建大文件的脚本来将磁盘几乎写满，接着再启动 Apache IoTDB，这样一定程度上可以快速复现此场景。然而，这需要测试人员开发并迭代此类效率工具，无疑也需要一定的时间成本。因此最好能有一个系统化的平台，使得一条命令或者一次点击就能够模拟出磁盘写满的场景，这将进一步降低人力成本。

### 三节点分布式中拔掉 1 节点网线后其他节点的读写延时均受影响

Apache IoTDB 基于一致性哈希和 Multi-Raft 设计实现了分布式版本。对于三节点的分布式，其设计的初衷便是能够容忍一个节点的宕机而不影响其他节点的读写。

目前用户跟社区反馈了一个 bug：对于三节点的分布式，直接关闭 1 个节点并不会影响其他两个节点的读写延时；然而如果物理上拔掉一个节点的网线，则直接会影响其他两个节点的读写延时。对于这种 case，测试人员在模拟这种场景时付出的成本会更高，因为需要找到可以操作的物理机才可以模拟出用户的场景，而且也需要开发人员同时配合 debug。

对于这种 case，我们实际上可以直接在操作系统层进行模拟。比如我们可以利用 tc 命令指定某一网卡对某一特定 ip 和端口实现 100% 的丢包，这样的方式可以使得上层感知的效果和物理拔网线基本一致。对于链路层的 tcp 客户端，请求都是有去无回，只能在超时后单方面决定向上层返回无法连接的错误。

当然，如果能有一个系统化的平台，使得一条命令或者一次点击就能够模拟出这种场景，这将进一步降低人力成本。

### 分布式稳定性

对于一个成熟的系统，其一定经过无数次生产环境和测试环境的验证。虽然理论上说 bug 不可避免。但我们依然可以通过我们的努力在测试环境发现尽可能多的 bug，从而降低生产环境产生 bug 的可能性，进而降低用户和我们自身名誉及财产受损的可能性。

对于一个分布式系统，我们需要考虑许多种异常情况下系统的对外表现，这些异常情况包括但不限于机器断电，进程假死/异常退出，网络分区/中断，磁盘故障/写满等等。对于任何一个分布式系统，这是一件几乎做不完的事情，但却是一件必须要做的事情。

2021 年 4 月，Apache IoTDB 发布了第一个分布式版本 0.12.0，尽管目前已有一定数量的 UT/IT 和 E2E 测试在 CI/CD 持续集成中保障着基本功能的稳定性，但我们还需要探索并优化各种 corner case 下系统的稳定性。

因此，拥有一个能够注入各种异常场景的系统化平台将是一件催生 Apache IoTDB 不断成长的利器。我们可以不断的收集并测试各种异常的 corner case，进而迭代优化 Apache IoTDB 分布式的稳定性。

## 混沌工程

实际上，上文中一直在提到的理念，基本就是**混沌工程理论**。以下将简单介绍下混沌工程。

### 什么是混沌工程

混沌工程这一名词最早由 [混沌工程理论](https://principlesofchaos.org/) 一文提出。在 2010 年 Netflix 从物理机基础设施迁移到 AWS 的过程中，为保证 EC2 实例故障不会对业务造成影响，其团队开发出了杀 EC2 实例的工具，这便是**混沌工程**的雏形。在 2015 年社区发布《混沌工程理论》一文后，混沌工程开始快速发展。

混沌工程是在分布式系统上进行实验的学科，旨在提升系统容错性，建立系统抵御生产环境中发生不可预知问题的信心。"打不倒我的必使我强大"，尼采的这句话很好了诠释了混沌工程反脆弱的思想。

### 为什么需要混沌工程

分布式系统日益复杂，而且在系统逐渐云化的背景下，系统的稳定性受到很大的挑战。这里从四个角色来说明混沌工程的重要性。

* 对于架构师，可以验证系统架构的容错能力，比如验证现在提倡的面向失败设计的系统；
* 对于开发和运维，可以提高故障的应急效率，实现故障告警、定位、恢复的有效和高效性。
* 对于测试，可以弥补传统测试方法留下的空白，之前的测试方法基本上是从用户的角度去做，而混沌工程是从系统的角度进行测试，降低故障复发率。
* 对于产品和设计，通过混沌事件查看产品的表现，提升客户使用体验。所以说混沌工程面向的不仅仅是开发、测试，拥有最好地客户体验是每个人的目标 所以实施混沌工程，可以提早发现生产环境上的问题，并且可以以战养战，提升故障应急效率和可以使用体验，逐渐建设高可用的韧性系统。

### 混沌工程实施原则

* 第一条："建立一个围绕稳定状态行为的假说"，其包含两个含义，一个是定义能直接反应业务服务的监控指标，需要注意的是这里的监控指标并不是系统资源指标，比如 CPU、内存等，这里的监控指标是能直接衡量系统服务质量的业务监控。举个例子，一个调用延迟故障，请求的 RT 会变长，对上层交易量造成下跌的影响，那么这里交易量就可以作为一个监控指标。这条原则的另一个含义是故障触发时，对系统行为作出假设以及监控指标的预期变化。
* 第二条指模拟生产环境中真实的或有理论依据的故障场景，比如依赖的服务调用延迟、超时、异常等。
* 第三条建议在生产环境中运行实验，但也不是说必须在生产环境中执行，只是实验环境越真实，混沌工程越有价值，但如果知道系统在某个故障场景下不具备容灾能力，不可以执行此混沌实验，避免损失发生。
* 第四条，持续的执行才能持续的降低故障复发率和提前发现故障，所以需要持续的自动化运行试验。
* 最后一个，混沌工程很重要的一点是控制爆炸半径，也就是试验影响面，防止预期外的资损发生，可以通过环境隔离或者故障注入工具提供的配置粒度来控制。

## 框架调研

以上简单介绍了混沌工程，以下将调研两个市面上最流行的混沌测试框架。他们分别是 Alibaba 社区的 ChaosBlade 和 PingCAP 社区的 ChaosMesh。

### ChaosBlade

ChaosBlade 是阿里巴巴开源的一款遵循混沌工程原理和混沌实验模型的实验注入工具，帮助企业提升分布式系统的容错能力，并且在企业上云或往云原生系统迁移过程中业务连续性保障。

ChaosBlade 是内部 MonkeyKing 对外开源的项目，其建立在阿里巴巴近十年故障测试和演练实践基础上，结合了集团各业务的最佳创意和实践。

ChaosBlade 不仅使用简单，而且支持丰富的实验场景，场景包括：

* 基础资源：比如 CPU、内存、网络、磁盘、进程等实验场景；
* Java 应用：比如数据库、缓存、消息、JVM 本身、微服务等，还可以指定任意类方法注入各种复杂的实验场景；
* C++ 应用：比如指定任意方法或某行代码注入延迟、变量和返回值篡改等实验场景；
* Docker 容器：比如杀容器、容器内 CPU、内存、网络、磁盘、进程等实验场景；
* 云原生平台：比如 Kubernetes 平台节点上 CPU、内存、网络、磁盘、进程实验场景，Pod 网络和 Pod 本身实验场景如杀 Pod，容器的实验场景如上述的 Docker 容器实验场景；

将场景按领域实现封装成一个个单独的项目，不仅可以使领域内场景标准化实现，而且非常方便场景水平和垂直扩展，通过遵循混沌实验模型，实现 ChaosBlade CLI 统一调用。目前包含的项目如下：

* [chaosblade](https://github.com/chaosblade-io/chaosblade)：混沌实验管理工具，包含创建实验、销毁实验、查询实验、实验环境准备、实验环境撤销等命令，执行方式包含 CLI 和 HTTP 两种。提供完善的命令、实验场景、场景参数说明，操作简洁清晰。
* [chaosblade-spec-go](https://github.com/chaosblade-io/chaosblade-spec-go): 混沌实验模型 Golang 语言定义，便于使用 Golang 语言实现的场景都基于此规范便捷实现。
* [chaosblade-exec-os](https://github.com/chaosblade-io/chaosblade-exec-os): 基础资源实验场景实现。
* [chaosblade-exec-docker](https://github.com/chaosblade-io/chaosblade-exec-docker): Docker 容器实验场景实现，通过调用 Docker API 标准化实现。
* [chaosblade-operator](https://github.com/chaosblade-io/chaosblade-operator): Kubernetes 平台实验场景实现，将混沌实验通过 Kubernetes 标准的 CRD 方式定义，很方便的使用 Kubernetes 资源操作的方式来创建、更新、删除实验场景，包括使用 kubectl、client-go 等方式执行，而且还可以使用上述的 ChaosBlade CLI 工具执行。
* [chaosblade-exec-jvm](https://github.com/chaosblade-io/chaosblade-exec-jvm): Java 应用实验场景实现，使用 Java Agent 技术动态挂载，无需任何接入，零成本使用，而且支持卸载，完全回收 Agent 创建的各种资源。
* [chaosblade-exec-cplus](https://github.com/chaosblade-io/chaosblade-exec-cplus): C++ 应用实验场景实现，使用 GDB 技术实现方法、代码行级别的实验场景注入。

### ChaosMesh

ChaosMesh 是一个开源的云原生混沌工程平台，提供丰富的故障模拟类型，具有强大的故障场景编排能力，方便用户在开发测试中以及生产环境中模拟现实世界中可能出现的各类异常，帮助用户发现系统潜在的问题。ChaosMesh 提供完善的可视化操作，旨在降低用户进行混沌工程的门槛。用户可以方便地在 Web UI 界面上设计自己的混沌场景，以及监控混沌实验的运行状态。

ChaosMesh 作为业内领先的混沌测试平台，具备以下核心优势：

* 核心能力稳固：ChaosMesh 起源于 TiDB 的核心测试平台，发布初期即继承了大量 TiDB 已有的测试经验。
* 被充分验证：ChaosMesh 被众多公司以及组织所使用，例如腾讯和美团等；同时被用于众多知名分布式系统的测试体系中，例如 Apache APISIX 和 RabbitMQ 等。
* 系统易用性强：图形化操作和基于 Kubernetes 的使用方式，充分利用了自动化能力。
* 云原生：ChaosMesh 原生支持 Kubernetes 环境，提供了强悍的自动化能力。
* 丰富的故障模拟场景：ChaosMesh 几乎涵盖了分布式测试体系中基础故障模拟的绝大多数场景。
* 灵活的实验编排能力：用户可以通过平台设计自己的混沌实验场景，场景可包含多个混沌实验编排，以及应用状态检查等。
* 安全性高：ChaosMesh 具有多层次安全控制设计，提供高安全性。
* 活跃的社区：ChaosMesh 为全球知名开源混沌测试平台，CNCF 开源基金会孵化项目。
* 强大的扩展能力：ChaosMesh 为故障测试类型扩展和功能扩展提供了充分的扩展能力。

### 总结

通过对两者官方文档的仔细研究和分析，同时结合知乎上若干大佬的 [描述](https://www.zhihu.com/question/364324836/answer/967459860)，我们可以得知其各自具有自己的优势和劣势：

|  项目名称   | Star |  优势  | 劣势 |
|  ----  | ----  | ----  | ----  | 
| [ChaosBlade](https://github.com/chaosblade-io/chaosblade)  | 4k | 物理节点场景：一开始便基于物理节点设计，因而其基于物理节点的错误注入种类更丰富，不仅具有通常的，此外还具有图形化界面加持，对用户较为友好。 | 云原生场景：具有 [ChaosBlade-Operator](https://github.com/chaosblade-io/chaosblade-operator) 项目来提供 K8S 环境的混沌测试支持，但相比云原生混沌测试框架 ChaosMesh 略有不足。 |
| [ChaosMesh](https://github.com/chaos-mesh/chaos-mesh)  | 3.8k | 一开始便基本云原生场景设计，所以对于基于 K8S 平台的数据库会更友好。在实验编排，自动化等方面相比 ChaosBlade 有更深刻的思考。 |刚开始维护基于物理节点的注入工具 [Chaosd](https://github.com/chaos-mesh/chaosd)，但由于起步较晚，功能和文档没有 ChaosBlade 完善，缺少 JVM 的错误注入，缺少 DashBoard。 |

## 项目内容

### Proposal

在最开始申请项目的 [Proposal](https://docs.google.com/document/d/1q3m0oQAIQAZWAe96d9tXwFZqXpugY_p4orfuZ1u0Kfk/edit#heading=h.fllp4im0x3en) 中，给出了这样的 timeline：

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/2bf3119a9f7ed0e6af2c187c89b2e5e6/image.png)

总结一下便是中间完成基于物理节点的异常注入，结项时完成基于 K8S 平台的异常注入。

### 主要工作

随后经过与社区成员的沟通，鉴于当前 Apache IoTDB 在 K8S 平台支持得还不是很好，社区也没有相应的 IoTDB-Operator。因此如果要实现基于 K8S 平台对 Apache IoTDB 注入异常，更多精力会被耗费在如何使得 Apache IoTDB 跑在 K8S ，即 IoTDB-Operator 上，这与本项目的初衷（实现 Apache IoTDB 的分布式混沌测试平台，进而提升 Apache IoTDB 的稳定性）不符。

因此，本项目的主要工作便调整为两个阶段：
* 中期
    * 产出一份工作调研文档。（即本文档）
    * 搭建好一个可用的混沌测试平台。
    * 相关部署工具开发。
* 结项：
    * 设计若干测试样例并配合测试人员进行混沌测试。
    * 跟进迭代修复这期间发现的新 bug。

基于以上新的工作内容，毫无意外我们最终选择了 ChaosBlade 来作为我们的混沌测试平台。一方面，其对于物理节点的异常注入种类更为丰富，包括但不限于内存，CPU，磁盘，网络，JVM 等错误注入的能力，基本可以满足最开始提出的五种 corner case 的需求；另一方面，图形化管理平台也能够降低使用的门槛，便于社区之后对项目进行周期性的混沌测试，从而不断对项目进行迭代优化。

## 错误类型

ChaosBlade 支持丰富地错误注入类型，本小节将简单介绍 Apache IoTDB 目前可能会用到的错误注入，以便之后图形化界面操作时以做参考。

注：更详细更丰富地错误注入类型可以进一步参考 [官方文档](https://chaosblade-io.gitbook.io/chaosblade-help-zh-cn/)。

### CPU 

#### 负载

创建 CPU 负载的混沌实验，可以指定核数、具体核满载或者总 CPU 负载百分比。

旨在 CPU 在特定负载下，验证服务质量、监控告警、流量调度、弹性伸缩等能力。

**命令：**
```Shell
blade create cpu load [flags]
```

参数
```Shell
--timeout string   设定运行时长，单位是秒，通用参数
--cpu-count string     指定 CPU 满载的个数
--cpu-list string      指定 CPU 满载的具体核，核索引从 0 开始 (0-3 or 1,3)
--cpu-percent string   指定 CPU 负载百分比，取值在 0-100
```

**案例：**
```Shell
# 创建 CPU 满载实验
blade create cpu load

# 返回结果如下
{"code":200,"success":true,"result":"beeaaf3a7007031d"}

# code 的值等于 200 说明执行成功，其中 result 的值就是 uid。使用 top 命令验证实验效果
Tasks: 100 total,   2 running,  98 sleeping,   0 stopped,   0 zombie
%Cpu0  : 21.3 us, 78.7 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu1  : 20.9 us, 79.1 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  : 20.5 us, 79.5 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  : 20.9 us, 79.1 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

# 4 核都满载，实验生效，销毁实验
blade destroy beeaaf3a7007031d

# 返回结果如下
{"code":200,"success":true,"result":"command: cpu load --help false --debug false"}

# 指定随机两个核满载
blade create cpu load --cpu-count 2

# 使用 top 命令验证结果如下，实验生效
Tasks: 100 total,   2 running,  98 sleeping,   0 stopped,   0 zombie
%Cpu0  : 17.9 us, 75.1 sy,  0.0 ni,  7.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu1  :  3.0 us,  6.7 sy,  0.0 ni, 90.3 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  :  0.7 us,  0.7 sy,  0.0 ni, 98.7 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  : 19.7 us, 80.3 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

# 指定索引是 0，3 的核满载，核的索引从 0 开始
blade create cpu load --cpu-list 0,3

# 使用 top 命令验证结果如下，实验生效
Tasks: 101 total,   2 running,  99 sleeping,   0 stopped,   0 zombie
%Cpu0  : 23.5 us, 76.5 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu1  :  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  :  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  : 20.9 us, 79.1 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

# 指定索引 1 到 3 的核满载
blade create cpu load --cpu-list 1-3

Tasks: 102 total,   4 running,  98 sleeping,   0 stopped,   0 zombie
%Cpu0  :  2.4 us,  7.1 sy,  0.0 ni, 90.2 id,  0.0 wa,  0.0 hi,  0.3 si,  0.0 st
%Cpu1  : 20.0 us, 80.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  : 15.7 us, 78.7 sy,  0.0 ni,  5.7 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  : 19.1 us, 78.9 sy,  0.0 ni,  2.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

# 指定百分比负载
blade create cpu load --cpu-percent 60

# 可以看到 CPU 总的使用率达到 60%， 空闲 40%
Tasks: 100 total,   1 running,  99 sleeping,   0 stopped,   0 zombie
%Cpu(s): 15.8 us, 44.1 sy,  0.0 ni, 40.0 id,  0.0 wa,  0.0 hi,  0.1 si,  0.0 st
```

### 磁盘

#### 读写 io 负载

创建磁盘读写 io 负载的混沌实验，可以提升磁盘读写 io 负载，可以指定受影响的目录，也可以通过调整读写的块大小提升 io 负载，默认值是 10，单位是 M，块的数量固定为 100，即在默认情况下，写会占用 1G 的磁盘空间，读会固定占用 600M 的空间，因为读操作会先创建一个 600M 的固定大小文件，预计 3s 之内，在创建时写 io 会升高。

验证磁盘 io 高负载下对系统服务的影响，比如监控告警、服务稳定性等。

**命令：**
```Shell
blade create disk burn [flags]
```

**参数：**
```Shell
--path string      指定提升磁盘 io 的目录，会作用于其所在的磁盘上，默认值是 /
--read             触发提升磁盘读 IO 负载，会创建 600M 的文件用于读，销毁实验会自动删除
--size string      块大小，单位是 M, 默认值是 10，一般不需要修改，除非想更大的提高 io 负载
--timeout string   设定运行时长，单位是秒，通用参数
--write            触发提升磁盘写 IO 负载，会根据块大小的值来写入一个文件，比如块大小是 10，则固定的块的数量是 100，则会创建 1000M 的文件，销毁实验会自动删除
```

**案例：**
```Shell
# 在执行实验之前可先观察磁盘 io 读写负载
iostat -x -t 2

# 上述命令会 2 秒刷新一次读写负载数据，截取结果如下
Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
vda               0.00     2.50    0.00    2.00     0.00    18.00    18.00     0.00    1.25    0.00    1.25   1.25   0.25

# 主要观察 rkB/s、wkB/s、%util 数据。执行磁盘读 IO 负载高场景
blade create disk burn --read --path /home

# 执行 iostat 命令可以看到读负载增大，使用率达 99.9%。执行 blade destroy UID（上述执行实验返回的 result 值）可销毁实验。

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
vda               0.00     3.00  223.00    2.00 108512.00    20.00   964.73    11.45   50.82   51.19   10.00   4.44  99.90

# 销毁上述实验后，执行磁盘写 IO 负载高场景
blade create disk burn --write --path /home

# 执行 iostat 命令可以看到写负载增大，使用率达 90.10%。
Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
vda               0.00    43.00    0.00  260.00     0.00 111572.00   858.25    15.36   59.71    0.00   59.71   3.47  90.10

# 可同时执行读写 IO 负载场景，不指定 path，默认值是 /
blade create disk burn --read --write

# 通过 iostat 命令可以看到，整个磁盘的 io 使用率达到了 100%
Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
vda               0.00    36.00  229.50  252.50 108512.00 107750.00   897.35    30.09   62.70   53.49   71.07   2.07 100.00
```

#### 填充

创建磁盘填充的混沌实验，可以指定填充的目录和填充大小。

验证磁盘满下对系统服务的影响，比如监控告警、服务稳定性等。

**命令：**
```Shell
blade create disk fill [flags]
```

**参数：**
```Shell
--path string      需要填充的目录，默认值是 /
--size string      需要填充的文件大小，单位是 M，取值是整数，例如 --size 1024
--reserve string   保留磁盘大小，单位是 MB。取值是不包含单位的正整数，例如 --reserve 1024。如果 size、percent、reserve 参数都存在，优先级是 percent > reserve > size
--percent string   指定磁盘使用率，取值是不带%号的正整数，例如 --percent 80
--retain-handle    是否保留填充
--timeout string   设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 执行实验之前，先看下 /home 所在磁盘的大小
df -h /home

Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        40G  4.0G   34G  11% /

# 执行磁盘填充，填充 40G，即达到磁盘满的效果（可用 34G）
blade create disk fill --path /home --size 40000

# 返回结果
{"code":200,"success":true,"result":"7a3d53b0e91680d9"}

# 查看磁盘大小
df -h /home

Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        40G   40G     0 100% /

# 销毁实验
blade destroy 7a3d53b0e91680d9

{"code":200,"success":true,"result":"command: disk fill --debug false --help false --path /home --size 40000"}

# 查看磁盘大小
df -h /home

Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        40G  4.0G   34G  11% /

# 执行按百分比填充磁盘，并且保留填充磁盘的文件句柄
blade c disk fill --path /home --percent 80 --retain-handle

{"code":200,"success":true,"result":"f1fa65e70950d0eb"}

df -h
/dev/vda1        40G   30G  8.1G  79% /

# 查看文件句柄
lsof /home/chaos_filldisk.log.dat
COMMAND     PID USER   FD   TYPE DEVICE    SIZE/OFF   NODE NAME
chaos_fil 19297 root    3r   REG  253,1 17697865728 394174 /home/chaos_filldisk.log.dat

# 执行保留固定大小实验场景
blade c disk fill --path /home --reserve 1024

df -h
Filesystem      Size  Used Avail Use% Mounted on

/dev/vda1        40G   37G  1.1G  98% /
```

### 内存

#### 占用

创建内存占用的混沌实验，可以指定内存占用，注意，此场景触发内存占用满，即使指定了 --timeout 参数，也可能出现通过 blade 工具无法恢复的情况，可通过重启机器解决！！！推荐指定内存百分比！

由于目前内存大小计算通过 memory.stat 等文件计算，所以和 free 命令计算不一致，同 top 命令一致，验证时请使用 top 命令查看内存使用。后续会针对内存占用场景进行优化。

**命令：**
```Shell
blade create mem load [flags]
```

**参数：**
```Shell
--mem-percent string    内存使用率，取值是 0 到 100 的整数
--mode string   内存占用模式，有 ram 和 cache 两种，例如 --mode ram。ram 采用代码实现，可控制占用速率，优先推荐此模式；cache 是通过挂载 tmpfs 实现；默认值是 --mode cache
--reserve string    保留内存的大小，单位是 MB，如果 mem-percent 参数存在，则优先使用 mem-percent 参数
--rate string 内存占用速率，单位是 MB/S，仅在 --mode ram 时生效
--timeout string   设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 在执行命令之前，先使用 top 命令查看内存使用信息，如下，总内存大小是 8G，使用了 7.6%
KiB Mem :  7.6/8010196  

# 执行内存占用 50%
blade c mem load --mode ram --mem-percent 50

# 查看内存使用
KiB Mem : 50.0/8010196 

# 执行内存占用 100%
KiB Mem : 99.6/8010196

# 保留 200M 内存，总内存大小 1G
blade c mem load --mode ram --reserve 200 --rate 100
KiB Mem :  1014744 total,    78368 free,   663660 used,   272716 buff/cache
KiB Swap:        0 total,        0 free,        0 used.   209652 avail Mem
KiB Mem : 79.7/1014744  [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||                   ]
```

### 网络

#### 延迟

创建网络延迟的混沌实验，可以指定网卡、本地端口、远程端口、目标 IP 延迟。需要特别注意，如果不指定端口、ip 参数，而是整个网卡延迟，切记要添加 --timeout 参数或者 --exclude-port 参数，前者是指定运行时间，自动停止销毁实验，后者是指定排除掉的延迟端口，两者都是防止因延迟时间设置太长，造成机器无法连接的情况，如果真实发生此问题，重启机器即可恢复。

本地端口和远程端口之间是或的关系，即这两个端口都会发生延迟，只要指定了本地端口或者远程端口，无需指定需要排除的端口。端口与 IP 之间是与的关系，即指定的 IP:PORT 发生延迟。

网络延迟场景主要验证网络异常的情况下，系统的自我容错能力。

**命令：**
```Shell
blade create network delay [flags]
```

**参数：**
```Shell
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--exclude-port string     排除掉的端口，默认会忽略掉通信的对端端口，目的是保留通信可用。可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--exclude-ip string       排除受影响的 IP，支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--interface string        网卡设备，例如 eth0 （必要参数）
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--offset string           延迟时间上下浮动的值，单位是毫秒
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--time string             延迟时间，单位是毫秒 （必要参数）
--force                   强制覆盖已有的 tc 规则，请务必在明确之前的规则可覆盖的情况下使用
--ignore-peer-port        针对添加 --exclude-port 参数，报 ss 命令找不到的情况下使用，忽略排除端口
--timeout string          设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 访问本机 8080 和 8081 端口延迟 3 秒，延迟时间上下浮动 1 秒
blade create network delay --time 3000 --offset 1000 --interface eth0 --local-port 8080,8081

{"code":200,"success":true,"result":"9b4aa9fabe073624"}

# 可以在另一台相同网络内的机器通过 telnet 命令验证，即 telnet xxx.xxx.xxx.xxx 8080
# 销毁实验
blade destroy 9b4aa9fabe073624

# 本机访问外部 14.215.177.39 机器（ping www.baidu.com 获取到的 IP）80 端口延迟 3 秒
blade create network delay --time 3000 --interface eth0 --remote-port 80 --destination-ip 14.215.177.39

# 可在本机通过 telnet 14.215.177.39 80 命令验证
# 对整个网卡 eth0 做 5 秒延迟，排除 22 和 8000 到 8080 端口
blade create network delay --time 5000 --interface eth0 --exclude-port 22,8000-8080

# 会发现 22 端口和 8000 到 8080 端口不受影响，可在另一台相同网络内的机器通过分别 telnet xxx.xxx.xxx.xxx 8080 和 telnet xxx.xxx.xxx.xxx 8081 进行测试
```

#### 丢包

创建网络丢包的混沌实验，可以指定网卡、本地端口、远程端口、目标 IP 丢包。需要特别注意，如果不指定端口、ip 参数，而是整个网卡丢包，切记要添加 --timeout 参数或者 --exclude-port 参数，前者是指定运行时间，自动停止销毁实验，后者是指定排除掉的丢包端口，两者都是防止因丢包率设置太高，造成机器无法连接的情况，如果真实发生此问题，重启机器即可恢复。

本地端口和远程端口之间是或的关系，即这两个端口都会发生丢包，只要指定了本地端口或者远程端口，无需指定需要排除的端口。端口与 IP 之间是与的关系，即指定的 IP:PORT 发生丢包。

网络丢包场景主要验证网络异常的情况下，系统的自我容错能力。

**命令：**
```Shell
blade create network loss [flags]
```

**参数：**
```Shell
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--exclude-port string     排除掉的端口，默认会忽略掉通信的对端端口，目的是保留通信可用。可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--exclude-ip string       排除受影响的 IP，支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--interface string        网卡设备，例如 eth0 （必要参数）
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--percent string          丢包百分比，取值在 [0, 100] 的正整数 （必要参数）
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--force                   强制覆盖已有的 tc 规则，请务必在明确之前的规则可覆盖的情况下使用
--ignore-peer-port        针对添加 --exclude-port 参数，报 ss 命令找不到的情况下使用，忽略排除端口
--timeout string          设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 访问本机 8080 和 8081 端口丢包率 70%
blade create network loss --percent 70 --interface eth0 --local-port 8080,8081

{"code":200,"success":true,"result":"b1cea124e2383848"}

# 可以在另一台相同网络内的机器通过 curl 命令验证，即 curl  xxx.xxx.xxx.xxx:8080，不使用 telnet 的原因是 telnet 内部有重试机制，影响实验验证。如果将 percent 的值设置为 100，可以使用 telnet 验证。
# 销毁实验
blade destroy b1cea124e2383848

# 本机访问外部 14.215.177.39 机器（ping www.baidu.com 获取到的 IP）80 端口丢包率 100%
blade create network loss --percent 100 --interface eth0 --remote-port 80 --destination-ip 14.215.177.39

# 可在本机通过 curl 14.215.177.39 命令验证，会发现访问不通。执行 curl 14.215.177.38 是通的。
# 对整个网卡 eth0 做 60% 的丢包，排除 22 和 8000 到 8080 端口
blade create network loss --percent 60 --interface eth0 --exclude-port 22,8000-8080

# 会发现 22 端口和 8000 到 8080 端口不受影响，可在另一台相同网络内的机器通过分别执行多次 curl xxx.xxx.xxx.xxx:8080 和 telnet xxx.xxx.xxx.xxx:8081 进行测试

# 实现整个网卡不可访问，不可访问时间 20 秒。执行完成下面命令后，当前的网络会中断掉，20 秒后恢复。切记！！勿忘 --timeout 参数
blade create network loss --percent 100 --interface eth0 --timeout 20
```

#### 重排序

创建网络包重排序的混沌实验，可以指定网卡、本地端口、远程端口、目标 IP 包重排。需要特别注意，如果不指定端口、ip 参数，而是整个网卡包重复，切记要添加 --timeout 参数或者 --exclude-port 参数，前者是指定运行时间，自动停止销毁实验，后者是指定排除掉不受影响的端口，两者都是防止机器无法连接的情况，如果真实发生此问题，重启机器即可恢复。

本地端口和远程端口之间是或的关系，即这两个端口都会生效，只要指定了本地端口或者远程端口，无需指定需要排除的端口。端口与 IP 之间是与的关系，即指定的 IP:PORT 发生包重排。

网络包重排序场景主要验证网络异常的情况下，系统的自我容错能力。

**命令：**
```Shell
blade create network reorder [flags]
```

**参数：**
```Shell
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--exclude-port string     排除掉的端口，默认会忽略掉通信的对端端口，目的是保留通信可用。可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--exclude-ip string       排除受影响的 IP，支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--interface string        网卡设备，例如 eth0 （必要参数）
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--offset string           延迟时间上下浮动的值，单位是毫秒
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--correlation string      和上一包的相关性，取值在 0~100，必要参数，例如 --correlation 70
--gap string              包序列大小，取值是正整数，例如 --gap 5
--percent string          立即发送百分比，取值是不带%号的正整数，例如 --percent 50，（必要参数）
--time string             网络包延迟时间，单位是毫秒，默认值是 10，取值时正整数
--force                   强制覆盖已有的 tc 规则，请务必在明确之前的规则可覆盖的情况下使用
--ignore-peer-port        针对添加 --exclude-port 参数，报 ss 命令找不到的情况下使用，忽略排除端口
--timeout string          设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 访问指定的 ip 请求包乱序
blade c network reorder --correlation 80 --percent 50 --gap 2 --time 500 --interface eth0 --destination-ip 180.101.49.12

ping 180.101.49.12 -A

PING 180.101.49.12 (180.101.49.12) 56(84) bytes of data.
64 bytes from 180.101.49.12: icmp_seq=1 ttl=50 time=510 ms
64 bytes from 180.101.49.12: icmp_seq=2 ttl=50 time=9.66 ms
64 bytes from 180.101.49.12: icmp_seq=4 ttl=50 time=9.70 ms
64 bytes from 180.101.49.12: icmp_seq=3 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=6 ttl=50 time=10.0 ms
64 bytes from 180.101.49.12: icmp_seq=5 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=8 ttl=50 time=9.71 ms
64 bytes from 180.101.49.12: icmp_seq=7 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=10 ttl=50 time=9.72 ms
64 bytes from 180.101.49.12: icmp_seq=9 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=11 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=12 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=13 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=15 ttl=50 time=9.90 ms
64 bytes from 180.101.49.12: icmp_seq=14 ttl=50 time=509 ms
64 bytes from 180.101.49.12: icmp_seq=16 ttl=50 time=509 ms
```

#### 重复

创建网络包重复的混沌实验，可以指定网卡、本地端口、远程端口、目标 IP 包重复。需要特别注意，如果不指定端口、ip 参数，而是整个网卡包重复，切记要添加 --timeout 参数或者 --exclude-port 参数，前者是指定运行时间，自动停止销毁实验，后者是指定排除掉不受影响的端口，两者都是防止机器无法连接的情况，如果真实发生此问题，重启机器即可恢复。

本地端口和远程端口之间是或的关系，即这两个端口都会生效，只要指定了本地端口或者远程端口，无需指定需要排除的端口。端口与 IP 之间是与的关系，即指定的 IP:PORT 发生包重复。

网络包重复场景主要验证网络异常的情况下，系统的自我容错能力。

**命令：**
```Shell
blade create network duplicate [flags]
```

**参数：**
```Shell
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--exclude-port string     排除掉的端口，默认会忽略掉通信的对端端口，目的是保留通信可用。可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--exclude-ip string       排除受影响的 IP，支持通过子网掩码来指定一个网段的 IP 地址，例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32，也可以通过都号分隔多个参数，例如 192.168.1.1,192.168.2.1。
--interface string        网卡设备，例如 eth0 （必要参数）
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--offset string           延迟时间上下浮动的值，单位是毫秒
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--percent                 包重复百分比，取值是不带%号的正整数
--force                   强制覆盖已有的 tc 规则，请务必在明确之前的规则可覆盖的情况下使用
--ignore-peer-port        针对添加 --exclude-port 参数，报 ss 命令找不到的情况下使用，忽略排除端口
--timeout string          设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 访问指定的 ip 请求包损坏，百分比 80%
blade create network corrupt --percent 80 --destination-ip 180.101.49.12 --interface eth0

ping 180.101.49.12

64 bytes from 180.101.49.12: icmp_seq=64 ttl=50 time=9.94 ms
64 bytes from 180.101.49.12: icmp_seq=65 ttl=50 time=9.73 ms
64 bytes from 180.101.49.12: icmp_seq=65 ttl=50 time=9.74 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=66 ttl=50 time=9.77 ms
64 bytes from 180.101.49.12: icmp_seq=66 ttl=50 time=9.80 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=67 ttl=50 time=9.70 ms
64 bytes from 180.101.49.12: icmp_seq=67 ttl=50 time=9.71 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=68 ttl=50 time=11.0 ms
64 bytes from 180.101.49.12: icmp_seq=68 ttl=50 time=11.1 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=69 ttl=50 time=9.78 ms
64 bytes from 180.101.49.12: icmp_seq=69 ttl=50 time=9.78 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=70 ttl=50 time=9.70 ms
64 bytes from 180.101.49.12: icmp_seq=70 ttl=50 time=9.70 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=71 ttl=50 time=9.71 ms
64 bytes from 180.101.49.12: icmp_seq=71 ttl=50 time=9.71 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=72 ttl=50 time=9.73 ms
64 bytes from 180.101.49.12: icmp_seq=72 ttl=50 time=9.74 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=73 ttl=50 time=9.69 ms
64 bytes from 180.101.49.12: icmp_seq=73 ttl=50 time=9.73 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=74 ttl=50 time=9.73 ms
64 bytes from 180.101.49.12: icmp_seq=74 ttl=50 time=9.73 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=75 ttl=50 time=9.68 ms
64 bytes from 180.101.49.12: icmp_seq=75 ttl=50 time=9.68 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=76 ttl=50 time=10.0 ms
64 bytes from 180.101.49.12: icmp_seq=76 ttl=50 time=10.0 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=77 ttl=50 time=9.68 ms
64 bytes from 180.101.49.12: icmp_seq=77 ttl=50 time=9.80 ms (DUP!)
64 bytes from 180.101.49.12: icmp_seq=78 ttl=50 time=9.70 ms
64 bytes from 180.101.49.12: icmp_seq=79 ttl=50 time=9.70 ms
64 bytes from 180.101.49.12: icmp_seq=80 ttl=50 time=9.74 ms
```

### 进程

#### 退出

创建进程退出的混沌实验。此实验会指定进程号杀掉进程。支持命令行或者命令中进程匹配。

此实验可以验证程序的自愈能力，或者服务进程不存在时，系统的容错能力。

**命令：**
```Shell
blade create process kill [flags]
```

**参数：**
```Shell
--process string       进程关键词，会在整个命令行中查找
--process-cmd string   进程命令，只会在命令中查找
--count string      限制杀掉进程的数量，0 表示无限制
--signal string     指定杀进程的信号量，默认是 9，例如 --signal 15
--timeout string   设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 删除包含 SimpleHTTPServer 关键词的进程
blade create process kill --process SimpleHTTPServer

# 删除 java 进程
blade create process kill --process-cmd java

# 指定信号量和本地端口杀进程
blade c process kill --local-port 8080 --signal 15 

# 执行前
netstat -tanp | grep 8080
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      10764/java

# 执行后此进程已不存在
```

#### 暂停

创建进程暂停的混沌实验。此实验会暂停进程。支持命令行或者命令中进程匹配。

此实验可以验证程序 Hang 时，系统的容错能力。

**命令：**
```Shell
blade create process stop [flags]
```

**参数：**
```Shell
--process string       进程关键词，会在整个命令行中查找
--process-cmd string   进程命令，只会在命令中查找
--timeout string   设定运行时长，单位是秒，通用参数
```

**案例：**
```Shell
# 暂停包含 SimpleHTTPServer 关键词的进程
blade create process stop --process SimpleHTTPServer

# 暂停 java 进程
blade create process stop --process-cmd java
```

### JVM

#### OOM

创建 JVM 内存溢出的混沌实验。

**命令：**
```Shell
blade create jvm oom [flags]
```

**参数：**
```Shell
--area string        JVM 内存区，目前支持 [HEAP, NOHEAP, OFFHEAP]，必填项。用 Heap 来表示 Eden+Old，, 用 NOHEAP 来表示 metaspace，用 OFFHEAP 来表示堆外内存
--block string       指定对象大小，仅支持 HEAP 和 OFFHEAP 区，单位是 MB
--interval string    单位 ms, 默认 500 两次 oom 异常间的时间间隔，只有在非暴力模式才生效，可以减缓 gc 的频率，不用担心进程会无响应
--wild-mode string   默认 false, 是否开启暴力模式，如果是暴力模式，在 OOM 发生之后也不会释放之前创建的内存，可能会引起应用进程无响应
```

**案例：**

注入堆内存占用：
```Shell
blade c jvm oom --area HEAP --wild-mode true --process tomcat

{"code":200,"success":true,"result":"99b9228b9632e043"}
```

故障注入之前：
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/8ae0f83f2ce31c7879f42e618238f39c/image.png)

故障注入之后：
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/90da762d22fa5b4fad0fb08c90cafadb/image.png)

#### Full GC

创建 JVM 产生 Full GC 的混沌实验。

**命令：**
```Shell
blade create jvm full-gc [flags]
```

**参数：**

已于 8 月 4 日的 [ChaosBlade v1.3.0](https://github.com/chaosblade-io/chaosblade/releases/tag/v1.3.0) 版本中发布，但文档尚未更新。

**案例：**

```Shell
blade c jvm full-gc --process spring --effect-count 5 --interval 1000
{"code":200,"success":true,"result":"2ee474df48fe5f85"}
```
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/35bf8186aea354ea115579a91eb86d0a/image.png)

## 机器环境

目前初步选择了 4 台机器搭建了混沌测试环境：其中 31，32，33 三个节点组成分布式 IoTDB 集群，34 节点当做客户端 benchmark 节点。

|  IP   | 操作系统 |  CPU  | 内存 | 网卡 | 磁盘 |
|  ----  | ----  | ----  | ----  | ---- | ---- |
|  192.168.130.31  | Ubuntu 16.04.7  | Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz 6 核 12 线程  | 16GB  | 1000Mb/s | 系统盘 HDD，数据盘 SSD |
|  192.168.130.32  | Ubuntu 16.04.7  | Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz 6 核 12 线程  | 16GB  | 1000Mb/s | 系统盘 HDD，数据盘 SSD |
|  192.168.130.33  | Ubuntu 16.04.7  | Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz 6 核 12 线程  | 16GB  | 1000Mb/s | 系统盘 HDD，数据盘 SSD |
|  192.168.130.34  | Ubuntu 16.04.7  | Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz 6 核 12 线程  | 16GB  |1000Mb/s  | 系统盘 HDD，数据盘 SSD  |

## 部署方式

截止 2021 年 9 月 24 日，ChaosBlade-Box 图形化管理平台的 [最新版本](https://github.com/chaosblade-io/chaosblade-box/tags) 为 0.4.2。因而以下将介绍基于 0.4.2 版本的实际部署。

以下将介绍在上一小节提到的四台机器上部署 ChaosBlade-Box 平台的过程，以便之后在其他机器上部署。具体步骤也可以进一步参照其 [官网文档](https://github.com/chaosblade-io/chaosblade-box/releases/tag/v0.4.2)。

1. 连接第一台机器 192.168.130.32，安装 docker。
```
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```
2. 启动 docker 和 mysql 实例。
```
service docker start
docker run --rm -d -it \
                        -p 3306:3306 \
                        -e MYSQL_DATABASE=chaosblade \
                        -e MYSQL_ROOT_PASSWORD=iotdb \
                        --name mysql-5.6 mysql:5.6 \
                        --character-set-server=utf8mb4 \
                        --collation-server=utf8mb4_unicode_ci \
                        --default-time_zone='+8:00'
```
3. 下载并启动图形化界面系统。
```
wget https://chaosblade.oss-cn-hangzhou.aliyuncs.com/platform/release/0.4.2/chaosblade-box-web-0.4.2.jar

nohup java -Duser.timezone=Asia/Shanghai -jar chaosblade-box-web-0.4.2.jar --spring.datasource.url="jdbc:mysql://192.168.130.32:3306/chaosblade?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai" --spring.datasource.username=root --spring.datasource.password=iotdb > chaosblade-box.log 2>&1 &
```

4. 打开 http://192.168.130.32:8080/ 进入图形化界面，如下图所示。
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/4aa5b237d3388ce50bb6c513ed046c7c/image.png)

5. 利用 SSH 的方式注册好所有机器。 

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/9a9728d2fe0432428e19bfcf716219a7/image.png)

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/7a57d0a394525fe25003a7ff6ec294ad/image.png)

6. 进入每个节点，参照[官网文档](https://www.yuque.com/docs/share/bc9ad412-6f96-463b-b72d-6773b5fb5ea3#WP5n5)手动下载演练工具。

```
cd /opt
wget https://chaosblade.oss-cn-hangzhou.aliyuncs.com/platform/chaostoolsctl.sh -O chaostoolsctl.sh && chmod +x chaostoolsctl.sh && ./chaostoolsctl.sh install -n chaosblade -v 1.0.0  -r https://chaosblade.oss-cn-hangzhou.aliyuncs.com/agent/github/1.0.0/chaosblade-1.0.0-linux-amd64.tar.gz
```

7. 部署成功。

## Apache IoTDB 部署工具

在实现过程中，为了方便部署分布式 Apache IoTDB，开发了效率部署工具，其不仅支持一键部署，还支持一键清理环境，一键拉取 log 到本地以进行分析等功能。相关用法可以参考 [deploy](./deploy) 目录下的 README。

## 用例调研

### TiDB

[TiDB](https://github.com/pingcap/tidb) 是一款最近几年新兴的 NewSQL 数据库，其集群架构具有 TiDB, PD 和 TiKV 三个组件，因此其混沌测试的用例会相对复杂一些。以下展示了其利用 ChaosMesh 和 Jepsen 的测试用例。

可以看到，其主要进行了时钟漂移，网络分区，节点宕机，节点假死，集群扩展等方面的混沌测试。

#### Jepsen

可参考[此处](https://github.com/jepsen-io/jepsen/tree/main/tidb)。

* **none**: no nemesis
* **clock-skew**: randomized clock skew and strobes
* **kill**: kills random processes
* **kill-db**: kill TiDB only
* **kill-pd**: kill PD only
* **kill-kv**: kill TiKV only
* **partition**: network partitions
* **partition-half**: n/2+1 splits
* **partition-one**: isolate single nodes
* **partition-ring**: each node can see separate, intersecting majorities
* **pause**: process pauses
* **pause-pd**: pause only PD
* **pause-kv**: pause only TiKV
* **pause-db**: pause only TiDB
* **random-merge**: merge partitions
* **restart-kv-without-pd**: restart KV nodes without PD available
* **schedules**: use debugging schedules in TiDB
* **shuffle-leader**: randomly reassign TiDB leaders
* **shuffle-region**: randomly reassign TiDB regions

#### ChaosMesh

可参考[此处](https://github.com/pingcap/tipocket)。

* **random_kill, all_kill, minor_kill, major_kill, kill_tikv_1node_5min, kill_pd_leader_5min**: As their name implies, these nemeses inject unavailable in a specified period of time.
* **short_kill_tikv_1node, short_kill_pd_leader**: Kill selected container, used to inject short duration of unavailable fault.
* **partition_one**: Isolate single nodes
* **scaling**: Scale up/down TiDB/PD/TiKV nodes randomly
* **shuffle-leader-scheduler/shuffle-region-scheduler/random-merge-scheduler**: Just as there name implies
* **small_skews, subcritical_skews, critical_skews, big_skews, huge_skews: Clock skew, small_skews ~100ms, subcritical_skews ~200ms, critical_skews ~250ms, big_skews ~500ms and huge_skews ~5s**

### CockroachDB

[CockroachDB](https://github.com/cockroachdb/cockroach) 也是一款近几年新兴的 NewSQL 数据库。以下展示了其利用 Jepsen 的测试用例。

可以看到，其主要进行了时钟漂移，网络分区，节点宕机，节点重启，网络延迟等方面的混沌测试。

#### Jepsen

可参考[此处](https://github.com/jepsen-io/jepsen/tree/main/cockroachdb)。

* **none**: no nemesis
* **small-skews**: clock skews of ~100 ms
* **subcritical-skews**: clock skews of ~200 ms
* **critical-skews**: clock skews of ~250 ms
* **big-skews**: clock skews of ~500 ms
* **huge-skews**: clock skews of ~5 s
* **strobe-skews**: strobes the clock at a high frequency between 0 and 200 ms ahead
* **parts**: random network partitions
* **majority-ring**: random network partition where each node sees a majority of other nodes
* **start-stop-2**: db processes on 2 nodes are stopped and restarted with SIGSTOP/SIGCONT
* **start-kill-2**: db processes on 2 nodes are stopped with SIGKILL and restarted from scratch
* **split**: periodically splits the keyrange at a randomly selected key
* **slow**: delays network packets

### 总结

基于以上调研结果，我们可以看到不同系统的混沌测试用例略有不同，但大同小异。主要集中在**节点宕机**，**节点假死**，**节点重启**，**网络分区**和**时钟漂移**五个方面。

## 测试方案

基于以上调研结果，我们可以给出第一阶段的混沌测试用例，先从生产环境最容易出现的场景出发，之后再不断补充。（注：时钟漂移的故障第一阶段暂不模拟）

* **节点宕机**：可以在读写过程中随机 kill 一个节点。该场景可以模拟生产环境某一节点宕机的情况。

* **节点假死**：可以在读写过程中随机 hang 一个节点。该场景可以模拟生产环境中某一节点长时间 FGC 的情况。

* **节点重启**：可以在读写过程中随机重启一个节点。该场景可以模拟生产环境中某一节点断电宕机后运维人员迅速重启恢复的情况。

* **网络分区**：可以在读写过程中对某节点进行网络分区。该场景可以模拟生产环境中某一节点的交换机/网线等网络设备暂时不可用的情况。

对于三节点三副本的分布式 IoTDB 集群，理想情况是可以容忍一个节点的故障。当一个节点出现故障时，可以有轻微的 liveness 问题（延迟轻度增加等），但不能有 safety 的问题（拒绝读写，数据错误等）。为了方便模拟，客户端默认均连接 32 节点，接着我们给 31 节点注入故障，从而进一步检测。

## 测试版本

* IoTDB : [1e12e6581b03b40956184a610063c2198466afbd](https://github.com/apache/iotdb/commit/1e12e6581b03b40956184a610063c2198466afbd)
* IoTDB-Benchmark : [9174d94bc09f8de16e67cc1c0716392244b216de](https://github.com/thulab/iotdb-benchmark/commit/9174d94bc09f8de16e67cc1c0716392244b216de)
* ChaosBlade-Box : [27c539f6fb02e753f6ca7bf4a69776b9cd0120ae](https://github.com/chaosblade-io/chaosblade-box/commit/27c539f6fb02e753f6ca7bf4a69776b9cd0120ae) 

## 测试参数

### IoTDB

```
default_replica_num = 3
```
其余均使用默认参数。

### IoTDB-Benchmark

```
DB_SWITCH: IoTDB-012-SESSION_BY_TABLET
OPERATION_PROPORTION: 1:0:0:0:0:0:0:0:0:0:0
ENABLE_THRIFT_COMPRESSION: false
INSERT_DATATYPE_PROPORTION: 1:1:1:1:1:1
IS_CLIENT_BIND: true
CLIENT_NUMBER: 20
GROUP_NUMBER: 20
DEVICE_NUMBER: 200
SENSOR_NUMBER: 500
BATCH_SIZE_PER_WRITE: 10
LOOP: 1000
POINT_STEP: 5000
QUERY_INTERVAL: 250000
IS_OUT_OF_ORDER: false
OUT_OF_ORDER_MODE: 0
OUT_OF_ORDER_RATIO: 0.5
```
其余均使用默认参数。

## 测试结果

### Baseline

所有节点正常工作，无任何错误注入时的 baseline 如下：

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/d72da7149bc575d073c20efd10c57dc3/image.png)


### 节点宕机

* 方法：使用 chaosblade-box 注入**进程退出**故障即可。
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/dea867f215970ca3b9e601b867bec602/image.png)

* 结果：注入后客户端存在报错，读写速度几乎降为 0。

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/95718420549f68455783cbe6a9fa43df/image.png)

* 相关进展：
    * 已记录 jira issue [1754](https://issues.apache.org/jira/projects/IOTDB/issues/IOTDB-1754?filter=allopenissues)。
    * 已修复一个节点频繁请求超时会导致其宕机的 [bug](https://github.com/apache/iotdb/pull/3579)。

### 节点假死

* 方法：使用 chaosblade-box 注入**进程暂停**故障即可。
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/0bebe1a7eb6a2dbe41fbd1ed527b79fc/image.png)

* 结果：注入后客户端存在报错，读写速度几乎降为 0。

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/5a4c6d576af82086b39de23dcda6f7b9/image.png)
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/6b70e7fc87687214455a63ff29087a21/image.png)

* 相关进展：
    * 已记录 jira issue [1755](https://issues.apache.org/jira/projects/IOTDB/issues/IOTDB-1755?filter=allopenissues)。


### 节点重启

* 方法：写数据过程达到 50% 左右时直接**手动重启节点**即可。

* 结果：cli 可以连接 31 节点但无法写入新存储组的数据；benchmark 中持续运行的 session 无法继续写入且爆出错误。

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/72c7921ee5e7fed87f99b9d624389a50/image.png)
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/aeeed5fc802dec84abef1659bb6c218e/image.png)

* 相关进展：
    * 本 bug 已记录为 jira issue [1756](https://issues.apache.org/jira/projects/IOTDB/issues/IOTDB-1756?filter=allopenissues)。
    * 已修复一个重启后无法读写的 [bug](https://github.com/apache/iotdb/pull/3939)。
    * 已修复一个重启后数据丢失的 [bug](https://github.com/apache/iotdb/pull/3930)。

### 网络分区

* 方法：写数据过程达到 50% 左右时使用 chaosblade-box 注入**网络丢包**故障即可。注意需指定好受影响的 port 和 ip。

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/148e14f2b47e4071a0c307843b681105/image.png)

* 结果：注入后客户端无报错，但读写速度几乎降为 0。服务器后台有错误日志。

![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/216a4eaae1349a9ee45cac633b638c4f/image.png)
![](https://gitlab.summer-ospp.ac.cn/summer2021/210070607/uploads/a64427863d4443548adc9c36cad228c1/image.png)

* 相关进展：
    * 已记录 jira issue [1757](https://issues.apache.org/jira/projects/IOTDB/issues/IOTDB-1757?filter=allopenissues)。

## 项目总结

### 项目产出

本项目的预期产出为：

* 产出一份工作调研文档。（即本文档）
* 搭建好一个可用的混沌测试平台。
* 相关部署工具开发。
* 设计若干测试样例并配合测试人员进行混沌测试。
* 跟进迭代修复这期间发现的新 bug。

截止 2021 年 9 月 28 日，除了最后一个工作目前和未来都一直在做以外，其他所有工作均已完成：
* 已产出了一份**详细的工作文档**，从需求分析，到工作调研，再到具体测试均有涉及。
* 已搭建好一个**方便好用**的混沌测试平台，可以通过 UI 的方式来注入错误，极大的降低了入门门槛。
* 开发实现了 Apache IoTDB 快速部署工具开发，支持**一键部署，一键清理资源和一键拉取日志**，极大的降低了部署分布式 IoTDB 的门槛。
* 设计出了第一版测试用例，从生产环境最容易出现的 case 出发，总结出了 **节点宕机**，**节点假死**，**节点重启** 和 **网络分区** 4 种测试用例并在 chaosblade-box 中进行了用例实现。
* 自 7 月 1 日以来，经过近三个月的调研和开发，本人深度参与了 Apache IoTDB 分布式的 bug 修复和功能迭代，共提交了 16 个 PR 且已被合并 15 个，review 了 30 个 PR。Apache IoTDB 分布式的稳定性相比 7 月份已有了很大的提升，目前也已经收获了第一个生产用户。


### 遇到的问题及解决方案

本项目遇到的主要问题都在 ChaosBlade-Box 的部署上，这主要是官方文档不齐全，网上相关资料很稀缺的缘故。主要遇到了以下两个问题，但最终都得以解决：
* 自动安装不成功：联系社区后解决。原因是官方文档有误解，即使利用 ssh 自动安装后依然需要手动安装 chaosagent。
* 节点 ip 加载不正确：自行尝试修改数据库后解决。原因是测试机器均为双网卡，然而上每个节点的 ip 加载不一致，导致往远端节点注入故障会连接超时。最终尝试手动更新 mysql 数据库中的节点 ip 后成功解决。

### 项目完成质量

本项目的初衷是：**实现 Apache IoTDB 的分布式混沌测试平台，进而提升 Apache IoTDB 的稳定性**。就目前来看，本项目已经实现了 Apache IoTDB 的分布式混沌测试平台，设计了若干生产环境最常见的测试用例并发现了 Apache IoTDB 的若干问题并对部分进行了修复。

本项目进一步明确了分布式 IoTDB 下一步的工作方向，希望通过这个平台，我们可以不断提升 Apache IoTDB 的稳定性，保障用户的数据安全！

### 导师沟通

在本次的项目过程中，我的导师在 IoTDB 的分布式代码具体实现上给予了我很多指导，让我进一步熟悉了现有的 codebase，同时也在一些 bug 的修复上给予了许多建设性的意见。非常感谢导师的帮助~

## 参考资料

* [Apache IoTDB 开源代码库](https://github.com/apache/iotdb)
* [Apache IoTDB 文档](https://iotdb.apache.org/UserGuide/Master/QuickStart/QuickStart.html)
* [PRINCIPLES OF CHAOS ENGINEERING](https://principlesofchaos.org/)
* [Awesome Chaos Engineering](https://github.com/dastergon/awesome-chaos-engineering)
* [ChaosBlade 开源代码库](https://github.com/chaosblade-io/chaosblade)
* [ChaosBlade 文档](https://chaosblade-io.gitbook.io/chaosblade-help-zh-cn/)
* [ChaosBlade-box 图形化管理平台](https://github.com/chaosblade-io/chaosblade-box)
* [ChaosBlade: 阿里一个超级牛逼的混沌实验实施工具](https://juejin.cn/post/6844903879814053901)
* [混沌工程控制台试用版 0.0.1-alpha 使用演示](https://www.bilibili.com/video/BV1pf4y1r7N2?from=search&seid=7539934383113186848)
* [混沌工程介绍与实践](https://github.com/chaosblade-io/awesome-chaosblade/blob/master/articles/%E6%B7%B7%E6%B2%8C%E5%B7%A5%E7%A8%8B%E4%BB%8B%E7%BB%8D%E4%B8%8E%E5%AE%9E%E8%B7%B5.md)
* [ChaosMesh 开源代码库](https://github.com/chaos-mesh/chaos-mesh)
* [ChaosMesh 文档](https://chaos-mesh.org/website-zh/docs/next/)
* [Chaosd 开源代码库](https://github.com/chaos-mesh/chaosd)
* [Chaosd 文档](https://chaos-mesh.org/website-zh/docs/next/chaosd-overview)
* [Jepsen 开源代码库](https://github.com/jepsen-io/jepsen)
* [IoTDB-Benchmark 开源代码库](https://github.com/thulab/iotdb-benchmark)